import { combineReducers } from 'redux';
import timeline from './timeline-reducer.js';

const rootReducer = combineReducers({
  timeline,
});

export default rootReducer;

import React from 'react';

import Title from '../Title';
import Times from '../Times';
import AdditionalInfo from '../AdditionalInfo';

import './Description.css';

const Description = ({ from, to, title, ...props }) => (
  <div className="Event__Description">
    <Times from={from} to={to} />
    <div className="Event__Description__titleWrapper">
      <Title>{title}</Title>
    </div>
    <AdditionalInfo to={to} from={from} title={title} {...props} />
  </div>
);

export default Description;

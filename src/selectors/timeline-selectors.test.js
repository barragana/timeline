import { getTrackerEvents, getEventsByColumns, getEventsByColumnsFiltered } from './timeline-selectors';

const state = {
  timeline: {
    trackerEvents: require('../../__mocks__/timeline.json'),
    filters: [],
  },
}

describe('timeline-selectors', () => {
  describe('getTrackerEvents should', () => {
    it('flat and map tracker events', () => {
      expect(getTrackerEvents(state)).toMatchSnapshot();
    });

    it('compute only once', () => {
      for(let i = 0; i < 1000; i++) {
        getTrackerEvents(state);
      }
      expect(getTrackerEvents.recomputations()).toEqual(1);
    });
  });

  describe('getEventsByColumns should', () => {
    it('group events by column in order to not overlap each other', () => {
      expect(getEventsByColumns(state)).toMatchSnapshot();
    });

    it('compute only once', () => {
      for(let i = 0; i < 1000; i++) {
        getEventsByColumns(state);
      }
      expect(getEventsByColumns.recomputations()).toEqual(1);
    });
  });

  describe('getEventsByColumnsFiltered should', () => {
    beforeEach(() => {
      getEventsByColumnsFiltered.resetRecomputations();
    })

    it('filter events grouped per columns by tracker', () => {
      const testState = { ...state };
      expect(getEventsByColumnsFiltered(testState)).toMatchSnapshot();

      testState.timeline.filters = ['google_calendar'];
      expect(getEventsByColumnsFiltered(testState)).not.toEqual(getEventsByColumnsFiltered(state));

      expect(
        getEventsByColumnsFiltered(testState)
      ).toMatchSnapshot();

      testState.timeline.filters = ['github'];
      expect(
        getEventsByColumnsFiltered(testState)
      ).toMatchSnapshot();

      testState.timeline.filters = ['macOS'];
      expect(
        getEventsByColumnsFiltered(testState)
      ).toMatchSnapshot();

      testState.timeline.filters = ['google_calendar', 'github'];
      expect(
        getEventsByColumnsFiltered(testState)
      ).toMatchSnapshot();
    });

    it('have same result even if events applied in different orders', () => {
      const result1 = getEventsByColumnsFiltered({ 
        ...state,
        timeline: { ...state.timeline, filters: ['github', 'google_calendar'] },
      });
      const result2 = getEventsByColumnsFiltered({ 
        ...state, 
        timeline: { ...state.timeline, filters: ['google_calendar', 'github'] },
      });

      expect(result2).toEqual(result1);
    })

    it('compute only when computation dit not happen yet', () => {
      const noFilter = getEventsByColumnsFiltered(state);
      expect(getEventsByColumnsFiltered.recomputations()).toEqual(1);

      const gcState = { ...state };
      const ghState = { ...state };
      const macOSState = { ...state };

      gcState.timeline.filters = ['google_calendar'];
      const gc = getEventsByColumnsFiltered(gcState);
      expect(noFilter).not.toEqual(gc);
      expect(getEventsByColumnsFiltered.recomputations()).toEqual(2);

      ghState.timeline.filters = ['github'];
      getEventsByColumnsFiltered(ghState);
      expect(getEventsByColumnsFiltered.recomputations()).toEqual(3);

      macOSState.timeline.filters = ['macOS'];
      getEventsByColumnsFiltered(macOSState);
      expect(getEventsByColumnsFiltered.recomputations()).toEqual(4);

      getEventsByColumnsFiltered(gcState);
      getEventsByColumnsFiltered(ghState);
      getEventsByColumnsFiltered(macOSState);
      getEventsByColumnsFiltered(state);
      expect(getEventsByColumnsFiltered.recomputations()).toEqual(4);
    });
  });
});

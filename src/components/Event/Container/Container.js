import React from 'react';

import './Container.css';

const Container = ({ children }) => (
  <div className="Event__Container">{children}</div>
);

export default Container;

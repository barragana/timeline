import React from 'react';

import './Title.css';

const Title = ({ children }) => (
  <span className="Event__Title">{children}</span>
);

export default Title;

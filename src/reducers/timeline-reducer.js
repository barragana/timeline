import * as types from '../actions/action-types';

const initialState = {
  trackerEvents: require('../fixtures/timeline.json'),
  date: '2017-12-05',
  items: [
    { name: 'Placeholder item 1' },
    { name: 'Placeholder item 2' },
    { name: 'Placeholder item 3' },
  ],
  filters: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.CHANGE_FILTER: {
      const index = state.filters.indexOf(action.payload.filter);
      const filters = [...state.filters];
      if (index > -1) {
        filters.splice(index, 1);
      } else {
        filters.push(action.payload.filter);
      }
      return {
        ...state,
        filters,
      };
    }
    default:
      return state;
  }
};

import { createSelector } from 'reselect';
import moment from 'moment';
import { eventFactory, eventsArrangement } from '../factories/timeline';

const readTrackerEvents = state => state.timeline.trackerEvents;
const readDate = state => state.timeline.date;
const getFilters = state => state.timeline.filters.sort();

export const getTimelineHours = createSelector([readDate], date => {
  return Array.from({ length: 25 }).map((value, index) => {
    return moment(date).add(index, 'hours');
  });
});

export const getTrackerEvents = createSelector(
  [readTrackerEvents], 
  trackerEvents => {
    return Object.keys(trackerEvents).reduce((existingEvents, trackerName) => {
      const events = trackerEvents[trackerName].map(event => {
        return eventFactory(event, trackerName);
      });
      return [...existingEvents, ...events];
    }, []).sort((a, b) => {
      if (a.position.top < b.position.top) {
        return -1;
      }
      if (a.position.top > b.position.top) {
        return 1;
      }
      return 0;
    });
});

export const getEventsByColumns = createSelector(
  getTrackerEvents,
  trackerEvents => {
    return eventsArrangement(trackerEvents);
});

export const getEventsByColumnsFiltered = createSelector(
  getFilters,
  getEventsByColumns,
  (filters, eventsByColumns) => {
    console.log('>>>>', filters);
    if (filters.length) {
      return eventsByColumns.map(eventsByColumn => (
        eventsByColumn.filter(event => filters.indexOf(event.tracker) > -1)
      ))
    }
    return eventsByColumns;
});

export const getTimelinePage = createSelector(
  getEventsByColumnsFiltered,
  getTimelineHours,
  getFilters,
  (events, hours, filters) => {
    return {
      events,
      hours,
      filters,
    };
  }
);

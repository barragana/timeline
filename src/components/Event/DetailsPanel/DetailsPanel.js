import React from 'react';

import Times from '../Times';
import DetailsInfo from '../DetailsInfo';

import './DetailsPanel.css';

const DetailsPanel = ({ title, subEntries, from, to }) => (
  <div className="Event__DetailsPanel">
    <DetailsInfo label="Title">{title}</DetailsInfo>
    <DetailsInfo label="Time"><Times to={to} from={from} /></DetailsInfo>
    {subEntries && subEntries.length && (
      <DetailsInfo label="Details">
        {subEntries.map(({ id, note }) => <div key={id}>- {note}</div>)}
      </DetailsInfo>
    )}
  </div>
);

export default DetailsPanel;

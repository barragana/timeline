import React from 'react';

import DetailsButton from '../DetailsButton';

import './AdditionalInfo.css';

const AdditionalInfo = ({ duration, onDetailsButtonClick, ...props }) => (
  <div className="Event__AdditionalInfo">
    <span className="Event__AdditionalInfo__duration">
      {!!duration.get('hours') && `${duration.get('hours')}h `}
      {!!duration.get('minutes') && `${duration.get('minutes')}m`}
    </span>
    <DetailsButton {...props} onClick={onDetailsButtonClick} />
  </div>
);

export default AdditionalInfo;

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getTimelinePage, makeGetTimelinePage } from './selectors/timeline-selectors';
import { changeFilter } from './actions/timeline-actions';
import Timeline from './components/Timeline';

import './App.css';

const mapStateToProps = state => getTimelinePage(state);

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ dispatchChangeFilter: changeFilter }, dispatch);
}

class App extends Component {
  render() {
    return (
      <Timeline 
        hours={this.props.hours}
        events={this.props.events}
        actions={{ changeFilter: this.props.dispatchChangeFilter }}
        filters={this.props.filters}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);

## Timeline

Basic application to display personal tracking timeline.

## Coding - Quick Overview

It is strongly suggested to use yarn, since I did not test app with npm.

```sh
yarn            # to install dependencies
yarn start      # to start
```
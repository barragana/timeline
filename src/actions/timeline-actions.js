import * as types from './action-types';

export const changeFilter = payload => {
  return {
    type: types.CHANGE_FILTER,
    payload,
  };
};

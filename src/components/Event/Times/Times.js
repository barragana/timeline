import React from 'react';

import './Times.css';

const Times = ({ from, to }) => (
  <span className="Event__Times">
    {from.format('HH:mm')} to {to.format('HH:mm')}
  </span>
);

export default Times;

import React from 'react';

import Title from '../Title';

import './HoverContainer.css';

const HoverContainer = ({ title }) => (
  <div className="Event__HoverContainer">
    <div className="Event__HoverContainer__titleWrapper">
      <Title>{title}</Title>
    </div>
  </div>
);

export default HoverContainer;

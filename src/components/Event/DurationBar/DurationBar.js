import React from 'react';
import cn from 'classnames';

import './DurationBar.css';

const DurationBar = ({ color, removeBottomBorder }) => (
  <div className={cn('Event__DurationBar', removeBottomBorder && 'Event__DurationBar--noBottomBorder')} style={{ backgroundColor: color }} />
);

export default DurationBar;

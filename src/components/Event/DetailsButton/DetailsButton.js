import React from 'react';
import cn from 'classnames';

import DetailsBalloon from '../DetailsBalloon';

import './DetailsButton.css';

const DetailsButton = ({ onClick, balloonOpened, ...props }) => (
  <div className="Event__DetailsButton__container">
    <button className={cn('Event__DetailsButton', balloonOpened && 'Event__DetailsButton--dark')} onClick={() => onClick(!balloonOpened)}>
      <svg className="Event__DetailsButton__svg" width="20" height="5" viewBox="0 0 20 5">
        <circle cx="17.5" cy="2.5" r="2.5" />
        <circle cx="10" cy="2.5" r="2.5" />
        <circle cx="2.5" cy="2.5" r="2.5" />
      </svg>
    </button>
    {balloonOpened && <DetailsBalloon {...props} onClick={onClick} />}
  </div>
);

export default DetailsButton;

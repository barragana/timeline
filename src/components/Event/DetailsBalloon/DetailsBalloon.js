import React from 'react';
import cn from 'classnames';

import DetailsPanel from '../DetailsPanel';

import './DetailsBalloon.css';

function transformX({ x, width }) {
  const { width: viewportWidth } = document.body.getBoundingClientRect();
  if ((x + width) > viewportWidth) {
    return '6';
  }

  if (x - (width / 2) < 0 && (x + width) < viewportWidth) {
    return '87';
  }

  return '47';
}

class DetailsBalloon extends React.Component {
  constructor(props) {
    super(props);
    this.ref = React.createRef();
    this.handleDocumentClick = this.handleDocumentClick.bind(this);

    this.state = { x: '47' };
  }

  componentDidMount() {
    const { x, width } = this.ref.current.getBoundingClientRect();
    this.setState({ x: transformX({ x, width }) })
    document.addEventListener('click', this.handleDocumentClick, false);
  }

  handleDocumentClick(e) {
    this.ref.current !== e.target && this.props.onClick(false);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleDocumentClick);
  }
  
  render() {
    return (
      <div
        className={cn('Event__DetailsBalloon', 'Event__DetailsBalloon__arrow', `Event__DetailsBalloon__arrow--transformXTo${this.state.x}`)}
        ref={this.ref}
        style={{ transform: `translateY(0px) translateX(${this.state.x}%)` }}
      >
        <DetailsPanel {...this.props} />
      </div>
    );
  }
}

export default DetailsBalloon;

import React, { Component } from 'react';
import moment from 'moment';
import cn from 'classnames';

import Event from './Event';
import './Timeline.css';

const FILTERS_OPTIONS = [
  'google_calendar',
  'github',
  'macOS',
];

class Timeline extends Component {
  renderHours() {
    return (
      <div className="hours">
        {this.props.hours.map((hourMoment, index) => {
          return (
            <div className="hour" key={index}>
              {moment(hourMoment).format('HH:mm')}
            </div>
          );
        })}
      </div>
    );
  }

  renderEvents() {
    return (
      <div className="timeline__background">
        <div className="timeline">
          {this.props.events.map((eventsByColumn, outerIndex) => {
            return (
              <div className="timeline__column" key={`timeline__column-${outerIndex}`}>
                {eventsByColumn.map((event, index) => {
                  return <Event style={{ ...event.position }} key={`${event.tracker}-${index}`} event={event} />;
                })}
              </div>
            )
          })}
        </div>
      </div>
    );
  }

  renderFilters() {
    return (
      <div className="timelineContainer__filters">
        {FILTERS_OPTIONS.map(filter => (
          <span 
            className={cn(
              'timelineContainer__filters__button',
              this.props.filters.indexOf(filter) > -1 && 'timelineContainer__filters__button--dark',
            )}
            onClick={() => this.props.actions.changeFilter({ filter })}
            key={filter}
          >
            <img
              src={require(`../images/${filter}.png`)}
              width="30"
              height="30"
              alt={`${filter.replace(/_/g, ' ')} filter icon`}
            />
          </span>
        ))}
      </div>
    );
  }

  render() {
    return (
      <div className="timelineContainer">
        {this.renderHours()}
        {this.renderEvents()}
        {this.renderFilters()}
      </div>
    );
  }
}

export default Timeline;

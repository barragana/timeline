import React from 'react';

import './DetailsInfo.css';

const DetailsInfo = ({ label, children }) => (
  <div className="Event__Details__Info">
    <span className="Event__Details__Info__label">{label}</span>
    {children}
  </div>
);

export default DetailsInfo;

import React from 'react';
import cn from 'classnames';

import DurationBar from './DurationBar';
import Icon from './Icon';
import Title from './Title';
import Description from './Description';
import Container from './Container';
import HoverContainer from './HoverContainer';

import './Event.css';

class Event extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      balloonOpened: false,
    }
    this.onDetailsButtonClick = this.onDetailsButtonClick.bind(this);
  }

  onDetailsButtonClick(balloonOpened) {
    this.setState({ balloonOpened });
  }

  render() {
    const { event, style } = this.props;
    return (
      <div id={event.id} className={cn('Event', this.state.balloonOpened && 'Event--zIndex100')} style={style}>
        <Container>
          <DurationBar color={event.color} removeBottomBorder={!event.finishSameDay} />
          {event.icon && <Icon icon={event.icon} />}
          <div className={cn(!event.showTitleAlways && 'Event--titleHidden')}>
            <Title>{event.title}</Title>
          </div>
          {event.isMainEvent && (
            <div className={cn((this.state.balloonOpened && 'Event__eventDescriptionWrapper--balloonOpened'), 'Event__eventDescriptionWrapper')}>
              <Description
                from={event.from}
                to={event.to}
                duration={event.duration.m}
                title={event.title}
                subEntries={event.subEntries}
                onDetailsButtonClick={this.onDetailsButtonClick}
                balloonOpened={this.state.balloonOpened}
              />
            </div>
          )}
        </Container>
        {event.isMainEvent && <HoverContainer title={event.title} />}
      </div>
    );
  }
}

export default Event;

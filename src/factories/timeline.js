import moment from 'moment';

const PIXEL_PER_MINUTE = 2;
const EVENT_MIN_HEIGHT = 11;

export const eventFactory = (event, tracker) => {
  const timeThreshold = moment(event.at || event.from).add(1, 'days').hour(0).minutes(50).seconds(0);
  const finishSameDay = moment(event.to || event.at || event.from).isSameOrBefore(timeThreshold);
  const position = calculatePosition(event, finishSameDay, timeThreshold);
  return {
    showTitleAlways: position.height > EVENT_MIN_HEIGHT,
    title: event.title,
    from: moment(event.at || event.from),
    to: moment(event.to),
    duration: event.duration && { ...event.duration, m: moment.duration(event.duration.formatted) },
    events: event.events || [],
    tracker,
    position,
    color: event.color,
    id: event.id,
    icon: (event.events && `${tracker}.png`) || event.icon,
    finishSameDay,
    isMainEvent: !event.events,
    subEntries: event.sub_entries,
  };
};

export const eventsArrangement = (trackerEvents) => {
  const eventsByColumns = [];
  const nextEventsAtColumn = []
  const eventsAtColumn = trackerEvents.reduce((events, event) => {
    const size = events.length;
    const previousEvent = (size > 0 && events[events.length - 1]) || { position: { height: 0, top: 0 }};
    if (event.position.top === previousEvent.position.top || event.position.top < (previousEvent.position.top + previousEvent.position.height)) {
      nextEventsAtColumn.push(event);
      return events;
    }
    return [...events, event];
  }, []);

  eventsByColumns.push(eventsAtColumn);
  if (nextEventsAtColumn.length) return eventsByColumns.concat(eventsArrangement(nextEventsAtColumn));
  return eventsByColumns;
}

function calculatePosition({ duration, at, from }, eventFinishSameDay, timeThreshold) {
  const date = moment(at || from);
  let height = (duration && duration.total_minutes * PIXEL_PER_MINUTE) || 0;

  if (!eventFinishSameDay) {
    height = timeThreshold.diff(date, 'minutes') * PIXEL_PER_MINUTE;
  }

  return {
    height: (height > EVENT_MIN_HEIGHT && height) || EVENT_MIN_HEIGHT,
    top: date.diff(date.clone().hour(0).minute(0).second(0), 'minutes') * PIXEL_PER_MINUTE,
  }
}

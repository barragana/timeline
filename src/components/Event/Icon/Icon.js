import React from 'react';

import './Icon.css';

const Icon = ({ icon }) => (
  <img src={require(`../../../images/${icon}`)} className="Event__Icon" alt="Event icon" />
);

export default Icon;
